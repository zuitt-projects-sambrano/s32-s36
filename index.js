const express = require('express');

const mongoose = require('mongoose');

// allows our backend application to be available in our frontend application
// cors - cross origin resource sharing
const cors = require('cors');

const userRoutes = require('./routes/userRoutes')

const courseRoutes = require('./routes/courseRoutes')

const port = 4000;

const app = express();

mongoose.connect("mongodb+srv://admin_sambrano:admin169@batch-169.gdgwg.mongodb.net/bookingAPI169?retryWrites=true&w=majority", {

    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection Error'));

db.once('open', () => console.log('Connected to MongoDB'));

app.use(express.json());

app.use(cors());

app.use('/users', userRoutes);

app.use('/courses', courseRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`))