const express = require('express');

const router = express.Router();

const courseControllers = require('../controllers/courseControllers');

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

router.post('/', verify, verifyAdmin, courseControllers.addCourse);

router.get('/', courseControllers.getAllCourses);

router.get('/getSingleCourse/:id', courseControllers.getSingleCourseController);

router.put('/archieve/:id', verify, verifyAdmin, courseControllers.archieve);

router.put('/activate/:id', verify, verifyAdmin, courseControllers.activate);

router.get('/getActiveCourses', courseControllers.getActiveCourses);

router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);

router.get('/getInactiveCourses', verify, verifyAdmin, courseControllers.getInactiveCourses);

router.post("/findCoursesByName", courseControllers.findCoursesByName);

router.post("/findCoursesByPrice", courseControllers.findCoursesByPrice);

router.get("/getEnrollees/:id", verify, verifyAdmin, courseControllers.getEnrollees);

module.exports = router;